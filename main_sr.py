import numpy as np
import torch as tr
import matplotlib.pyplot as plt
import utility
import imageio
import tkinter
import matplotlib

from pathlib import Path
from argparse import ArgumentParser
from tqdm import tqdm
from neural_wrappers.utilities import tryWriteImage, tryWriteVideo
from neural_wrappers.pytorch import npGetData, trGetData, device
from model import Model

matplotlib.use("TkAgg")

def srImage(model, img):
	npImg = np.float32(img).transpose(0, 3, 1, 2)
	res = model.npForward(npImg)
	if model.get_model().mode == "2x":
		res = res[1]
	elif model.get_model().mode == "4x":
		res = res[2]
	else:
		assert False, "Mode: %s" % (model.get_model().mode)
	npResImg = np.clip(res.transpose(0, 2, 3, 1), 0, 255).astype(np.int32)
	return npResImg

def doVideo(model, video, batchSize, quiet=True):
	res = []
	N = len(video) // batchSize + (len(video) % batchSize != 0)
	Range = range(N)
	if not quiet:
		Range = tqdm(Range)
	for i in Range:
		startIndex = i * batchSize
		endIndex = min((i + 1) * batchSize, len(video))
		imgs = video[startIndex : endIndex]
		npResImgs = srImage(model, imgs)
		res.extend(npResImgs)
	return np.array(res).astype(np.uint8)

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("pre_train")
	parser.add_argument("item_path")
	parser.add_argument("savePath")
	parser.add_argument("--video_batch_size", type=int, default=2)
	parser.add_argument("--mode", default="2x")

	args = parser.parse_args()
	args.scale = [pow(2, s+1) for s in range(int(np.log2([4])))]
	args.cpu = device
	args.model = "DRN-L"
	args.test_only = True
	args.rgb_range = 255
	args.self_ensemble = "store_true"
	args.n_GPUs = 1
	args.n_blocks = 30
	args.n_feats = 20
	args.n_colors = 3
	args.negval = 0.2
	args.pre_train_dual = "."
	return args

def main():
	args = getArgs()
	utility.set_seed(1)
	print("Device:", device)
	model = Model(args).to(device).eval()
	model.get_model().mode = args.mode

	if args.type == "image":
		img = imageio.imread(args.item_path)[..., 0 : 3] 
		npResImg = srImage(model, np.expand_dims(img, axis=0))[0]

		if args.savePath:
			tryWriteImage(npResImg, args.savePath)
		# if args.show:
		# 	ax = plt.subplots(1, 2)[1]
		# 	ax[0].imshow(img)
		# 	ax[1].imshow(npResImg)
		# 	plt.show()
	elif args.type == "video":
		reader = imageio.get_reader(args.item_path)
		fps = reader.get_meta_data()["fps"]
		video = np.array([item for item in reader])

		print(video.shape, video.min(), video.max(), video.dtype, fps)
		res = doVideo(model, video, args.video_batch_size, quiet=False)

		# if args.show:
		# 	for j in range(len(npResImgs)):
		# 		ax = plt.subplots(1, 2)[1]
		# 		ax[0].imshow(imgs[j])
		# 		ax[1].imshow(npResImgs[j])
		# 		plt.show()
		
		if args.savePath:
			tryWriteVideo(res, args.savePath, fps)
	elif args.type == "video_dir":
		path = Path(args.item_path)
		assert path.exists()
		vids = list(path.glob("*.mp4"))
		assert len(vids) > 0
		outPath = Path(args.savePath)
		outPath.mkdir(parents=True, exist_ok=True)

		for i in tqdm(range(len(vids))):
			vidPath = vids[i]
			reader = imageio.get_reader(vidPath)
			fps = reader.get_meta_data()["fps"]
			video = np.array([item for item in reader])
			videoSavePath = "%s/%s" % (args.savePath, str(vidPath).split("/")[-1])

			print(str(vidPath), videoSavePath, video.shape, video.min(), video.max(), video.dtype, fps)
			res = doVideo(model, video, args.video_batch_size)
			
			tryWriteVideo(res, videoSavePath, fps, quiet=True)

if __name__ == "__main__":
	main()
